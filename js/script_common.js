'use strict';

(function()
{
    const hamburgerButton = document.querySelector('#menu');

    hamburgerButton.addEventListener('click', function()
    {
        const expanded = this.getAttribute('aria-expanded') === "true";
        this.setAttribute('aria-expanded', !expanded + ""); 
    });
})()